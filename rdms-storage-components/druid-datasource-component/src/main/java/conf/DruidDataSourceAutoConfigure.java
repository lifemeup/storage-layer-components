package conf;

import com.alibaba.druid.filter.stat.StatFilter;
import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import com.alibaba.druid.wall.WallConfig;
import com.alibaba.druid.wall.WallFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.servlet.Servlet;
import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * @author MAX
 * @date 2020/7/21 1:30 下午
 */

@Configuration
@ConditionalOnClass(DruidDataSource.class)
@EnableConfigurationProperties({DruidDataSourceProperties.class})
@ConditionalOnProperty(name = "spring.datasource.type", havingValue = "com.alibaba.druid.pool.DruidDataSource", matchIfMissing = true)
public class DruidDataSourceAutoConfigure {

    private static final Logger logger = LoggerFactory.getLogger(DruidDataSourceAutoConfigure.class);

    @Autowired
    private DruidDataSourceProperties dataSourceProperties;

    @Primary
    @Bean(destroyMethod = "close", initMethod = "init")
    public DataSource dataSource() {
        DruidDataSource datasource = new DruidDataSource();
        if (logger.isDebugEnabled()) {
            logger.debug("default dataSource configuration: " + dataSourceProperties);
        }

        //基础配置
        datasource.setUrl(dataSourceProperties.getUrl());
        datasource.setDriverClassName(dataSourceProperties.getDriverClassName());
        datasource.setUsername(dataSourceProperties.getUsername());
        datasource.setPassword(dataSourceProperties.getPassword());

        datasource.setInitialSize(dataSourceProperties.getInitialSize());
        datasource.setMinIdle(dataSourceProperties.getMinIdle());
        datasource.setMaxActive(dataSourceProperties.getMaxActive());

        // 配置获取连接等待超时的时间
        datasource.setMaxWait(dataSourceProperties.getMaxWait());
        // 配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒
        datasource.setTimeBetweenEvictionRunsMillis(dataSourceProperties.getTimeBetweenEvictionRunsMillis());
        // 配置一个连接在池中最小生存的时间，单位是毫秒
        datasource.setMinEvictableIdleTimeMillis(dataSourceProperties.getMinEvictableIdleTimeMillis());
        datasource.setValidationQuery(dataSourceProperties.getValidationQuery());
        datasource.setTestWhileIdle(dataSourceProperties.isTestWhileIdle());
        datasource.setTestOnBorrow(dataSourceProperties.isTestOnBorrow());
        datasource.setTestOnReturn(dataSourceProperties.isTestOnReturn());
        datasource.setPoolPreparedStatements(dataSourceProperties.isPoolPreparedStatements());
        datasource.setMaxPoolPreparedStatementPerConnectionSize(dataSourceProperties.getMaxPoolPreparedStatementPerConnectionSize());
        datasource.setUseGlobalDataSourceStat(dataSourceProperties.isUseGlobalDataSourceStat());

        try {
            datasource.setFilters(dataSourceProperties.getFilters());
        } catch (SQLException e) {
            logger.error("druid configuration initialization filter", e);
        }

        if (logger.isDebugEnabled() && !dataSourceProperties.getConnectionProperties().contains("config.decrypt=true")) {
            logger.debug("advise to encrypt your database password," +
                    "use java -cp druid-x.x.xx.jar com.alibaba.druid.filter.config.ConfigTools your password.");
        }

        datasource.setConnectionProperties(dataSourceProperties.getConnectionProperties());
        return datasource;
    }

    @Bean
    @ConditionalOnProperty(prefix = "druid.monitor", name = "enable", havingValue = "true")
    public ServletRegistrationBean<Servlet> druidServlet() {
        printServerInfo();
        ServletRegistrationBean<Servlet> servletRegistrationBean = new ServletRegistrationBean<>();
        //配置一个拦截器
        servletRegistrationBean.setServlet(new StatViewServlet());
        //指定拦截器只拦截druid管理页面的请求
        servletRegistrationBean.addUrlMappings("/druid/*");
        HashMap<String, String> initParam = new HashMap<String, String>(5);
        //登录druid管理页面的用户名
        initParam.put("loginUsername", "admin");
        //登录druid管理页面的密码
        initParam.put("loginPassword", "admin");
        //是否允许重置druid的统计信息
        initParam.put("resetEnable", "true");
        //ip白名单，如果没有设置或为空，则表示允许所有访问
        initParam.put("allow", "");
        servletRegistrationBean.setInitParameters(initParam);
        return servletRegistrationBean;
    }

    private void printServerInfo() {
        logger.info("Druid DataSource Monitor starting...");
        logger.info("visit url: http://ip:port:druid/");
        logger.info("loginUsername: admin");
        logger.info("loginPassword: admin");
    }

    @Bean
    @ConditionalOnProperty(prefix = "druid.monitor", name = "enable", havingValue = "true")
    public FilterRegistrationBean<WebStatFilter> filterRegistrationBean() {
        FilterRegistrationBean<WebStatFilter> filterRegistrationBean = new FilterRegistrationBean<WebStatFilter>();
        filterRegistrationBean.setFilter(new WebStatFilter());
        filterRegistrationBean.addUrlPatterns("/*");
        filterRegistrationBean.addInitParameter("exclusions", "*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*");
        return filterRegistrationBean;
    }

    @Bean
    @ConditionalOnProperty(prefix = "druid.monitor", name = "enable", havingValue = "true")
    public StatFilter statFilter() {
        StatFilter statFilter = new StatFilter();
        statFilter.setSlowSqlMillis(3000);
        statFilter.setLogSlowSql(true);
        statFilter.setMergeSql(true);
        return statFilter;
    }

    /**
     * sql防火墙过滤器配置
     *
     * @param wallConfig
     * @return
     */
    @Bean
    @ConditionalOnProperty(prefix = "druid.monitor", name = "enable", havingValue = "true")
    public WallFilter wallFilter(WallConfig wallConfig) {
        WallFilter wallFilter = new WallFilter();
        wallFilter.setConfig(wallConfig);
        //对被认为是攻击的SQL进行LOG.error输出
        wallFilter.setLogViolation(true);
        //对被认为是攻击的SQL抛出SQLException
        wallFilter.setThrowException(false);
        return wallFilter;
    }

    /**
     * sql防火墙配置
     *
     * @return
     */
    @Bean
    @ConditionalOnProperty(prefix = "druid.monitor", name = "enable", havingValue = "true")
    public WallConfig wallConfig() {
        WallConfig wallConfig = new WallConfig();
        wallConfig.setAlterTableAllow(false);
        wallConfig.setCreateTableAllow(false);
        wallConfig.setDeleteAllow(false);
        wallConfig.setMergeAllow(false);
        wallConfig.setDescribeAllow(false);
        wallConfig.setShowAllow(false);
        return wallConfig;
    }

}
