
#### 1、配置参数说明
```properties
mongodb:
  database: bfa_mongo
  address: "host:port,host:port"
  username: "xxx"
  password: "xxxxx"
  authenticationDatabase: [设置你的认证数据库，如果有的话]
  # 连接池配置
  clientName: ${spring.application.name} # 客户端的标识，用于定位请求来源等
  connectionTimeoutMs: 10000     # TCP连接超时，毫秒
  readTimeoutMs: 15000       # TCP读取超时，毫秒
  poolMaxWaitTimeMs: 3000        #当连接池无可用连接时客户端阻塞等待的时长，单位毫秒
  connectionMaxIdleTimeMs: 60000   #TCP连接闲置时间，单位毫秒
  connectionMaxLifeTimeMs: 120000    #TCP连接最多可以使用多久，单位毫秒
  heartbeatFrequencyMs: 20000      #心跳检测发送频率，单位毫秒
  minHeartbeatFrequencyMs: 8000    #最小的心跳检测发送频率，单位毫秒
  heartbeatConnectionTimeoutMs: 10000  #心跳检测TCP连接超时，单位毫秒
  heartbeatReadTimeoutMs: 15000    #心跳检测TCP连接读取超时，单位毫秒
  connectionsPerHost: 20       # 每个host的TCP连接数
  minConnectionsPerHost: 5     #每个host的最小TCP连接数
  #计算允许多少个线程阻塞等待可用TCP连接时的乘数，算法：threadsAllowedToBlockForConnectionMultiplier*connectionsPerHost，当前配置允许10*20个线程阻塞
  threadsAllowedToBlockForConnectionMultiplier: 10
```
支持五种 read preference模式：
1.primary：主节点，默认模式，读操作只在主节点，如果主节点不可用，报错或者抛出异常。
2.primaryPreferred：首选主节点，大多情况下读操作在主节点，如果主节点不可用，如故障转移，读操作在从节点。
3.secondary：从节点，读操作只在从节点，如果从节点不可用，报错或者抛出异常。
4.secondaryPreferred：首选从节点，大多情况下读操作在从节点，特殊情况（如单主节点架构）读操作在主节点。
5.nearest：最邻近节点，读操作在最邻近的成员，可能是主节点或者从节点，关于最邻近的成员请参考。