package com.gzxx.mongo.entity;

import java.io.Serializable;
import java.util.List;

/**
 * @Description MongoDB数据分页实体
 * @Author pengpdx
 * @Date 2020/7/21 15:12
 */
public class PageModel implements Serializable {

    /**
     * 当前页
     */
    private Integer nowPage = 1;
    /**
     * 当前页条数
     */
    private Integer pagesize = 10;
    /**
     * 总共的条数
     */
    private Long total;
    /**
     * 总共的页数
     */
    private Integer pages;
    /**
     * 实体类集合
     */
    private List<?> list;

    public Integer getNowPage() {
        return nowPage;
    }

    public void setNowPage(Integer nowPage) {
        this.nowPage = nowPage;
    }

    public Integer getPagesize() {
        return pagesize;
    }

    public void setPagesize(Integer pagesize) {
        this.pagesize = pagesize;
    }

    public Long getTotal() {
        return total;
    }

    /**
     * 总数由0开始计数
     * @param total
     */
    public void setTotal(Long total) {
        this.total = total + 1;
    }

    public Integer getPages() {
        return pages;
    }

    public void setPages(Integer pages) {
        this.pages = pages;
    }

    public List<?> getList() {
        return list;
    }

    public void setList(List<?> list) {
        this.list = list;
    }

    public PageModel(Integer nowPage, Integer pagesize, Long total, Integer pages, List<?> list) {
        this.nowPage = nowPage;
        this.pagesize = pagesize;
        this.total = total;
        this.pages = pages;
        this.list = list;
    }

    public PageModel() {
    }

    @Override
    public String toString() {
        return "PageModel{" +
                "nowPage=" + nowPage +
                ", pagesize=" + pagesize +
                ", total=" + total +
                ", pages=" + pages +
                ", list=" + list +
                '}';
    }
}
