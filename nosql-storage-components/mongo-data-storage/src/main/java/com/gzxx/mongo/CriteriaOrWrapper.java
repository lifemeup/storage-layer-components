package com.gzxx.mongo;

import org.springframework.data.mongodb.core.query.Criteria;

import java.util.Collection;

/**
 * @Description 查询语句生成器 OR连接
 * @Author pengpdx
 * @Date 2020/7/22 11:07
 */
public class CriteriaOrWrapper extends CriteriaWrapper {

    public CriteriaOrWrapper() {
        andLink = false;
    }

    public CriteriaOrWrapper or(Criteria criteria) {
        list.add(criteria);
        return this;
    }

    public CriteriaOrWrapper or(CriteriaWrapper criteriaWrapper) {
        list.add(criteriaWrapper.build());
        return this;
    }

    /**
     * 等于
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaWrapper
     */
    public CriteriaOrWrapper eq(String column, Object params) {
        super.eq(column, params);
        return this;
    }

    /**
     * 不等于
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaOrWrapper
     */
    public CriteriaOrWrapper ne(String column, Object params) {
        super.ne(column, params);
        return this;
    }

    /**
     * 小于
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaOrWrapper
     */
    public CriteriaOrWrapper lt(String column, Object params) {
        super.lt(column, params);
        return this;
    }

    /**
     * 小于或等于
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaOrWrapper
     */
    public CriteriaOrWrapper lte(String column, Object params) {
        super.lte(column, params);
        return this;
    }

    /**
     * 大于
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaOrWrapper
     */
    public CriteriaOrWrapper gt(String column, Object params) {
        super.gt(column, params);
        return this;
    }

    /**
     * 大于或等于
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaOrWrapper
     */
    public CriteriaOrWrapper gte(String column, Object params) {
        super.gte(column, params);
        return this;
    }

    /**
     * 包含
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaOrWrapper
     */
    public CriteriaOrWrapper contain(String column, Object params) {
        super.contain(column, params);
        return this;
    }

    /**
     * 包含,以或连接
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaOrWrapper
     */
    public CriteriaOrWrapper containOr(String column, Collection<?> params) {
        super.containOr(column, params);
        return this;
    }

    /**
     * 包含,以或连接
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaOrWrapper
     */
    public CriteriaOrWrapper containOr(String column, Object[] params) {
        super.containOr(column, params);
        return this;
    }

    /**
     * 包含,以且连接
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaOrWrapper
     */
    public CriteriaOrWrapper containAnd(String column, Collection<?> params) {
        super.containAnd(column, params);
        return this;
    }

    /**
     * 包含,以且连接
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaOrWrapper
     */
    public CriteriaOrWrapper containAnd(String column, Object[] params) {
        super.containAnd(column, params);
        return this;
    }

    /**
     * 相似于
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaOrWrapper
     */
    public CriteriaOrWrapper like(String column, String params) {
        super.like(column, params);
        return this;
    }

    /**
     * 在其中
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaOrWrapper
     */
    public CriteriaOrWrapper in(String column, Collection<?> params) {
        super.in(column, params);
        return this;
    }

    /**
     * 在其中
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaOrWrapper
     */
    public CriteriaOrWrapper in(String column, Object[] params) {
        super.in(column, params);
        return this;
    }

    /**
     * 不在其中
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaOrWrapper
     */
    public CriteriaOrWrapper nin(String column, Collection<?> params) {
        super.nin(column, params);
        return this;
    }

    /**
     * 不在其中
     *
     * @param column 字段
     * @param params 参数
     * @return CriteriaOrWrapper
     */
    public CriteriaOrWrapper nin(String column, Object[] params) {
        super.nin(column, params);
        return this;
    }

    /**
     * 为空
     *
     * @param column 字段
     * @return CriteriaOrWrapper
     */
    public CriteriaOrWrapper isNull(String column) {
        super.isNull(column);
        return this;
    }

    /**
     * 不为空
     *
     * @param column 字段
     * @return CriteriaOrWrapper
     */
    public CriteriaOrWrapper isNotNull(String column) {
        super.isNotNull(column);
        return this;
    }
}
