package com.gzxx.mongo;

import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.List;

/**
 * @Description Sql日志打印
 * @Author pengpdx
 * @Date 2020/7/22 18:40
 */
public interface MongoSqlLogServer {

    void logQuery(Class<?> clazz, Query query);

    void logCount(Class<?> clazz, Query query);

    void logDelete(Class<?> clazz, Query query);

    void logUpdate(Class<?> clazz, Query query, Update update, boolean multi);

    void logSave(Object object);

    void logSave(List<?> list);
}
