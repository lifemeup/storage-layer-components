package com.redisdatastorage.utils;

import com.redisdatastorage.cache.RedisCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * RedisUtils
 *
 * @author chenjing
 */
@Component
public class RedisUtils {

    @Autowired
    private RedisCache redisCache;

    /**
     * 添加对象
     */
    public <T> void setObject(String key, T value) {
        redisCache.setObject(key, value);
    }

    /**
     * 添加对象 有过期时长
     */
    public <T> void setObject(String key, T value, long timeOut, TimeUnit timeUnit) {
        redisCache.setObject(key, value, timeOut, timeUnit);
    }

    /**
     * 获取对象
     */
    public <T> T getObject(String key, Class<T> clazz) {
        return (T) redisCache.getObject(key);
    }
}
