package com.redisdatastorage.cache;

import com.redisdatastorage.config.RedisProperties;
import io.lettuce.core.RedisClient;
import io.lettuce.core.RedisFuture;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.async.RedisAsyncCommands;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Point;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.RedisStringCommands;
import org.springframework.data.redis.core.*;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * RedisCache
 *
 * @author chenjing
 */
@Component
public class RedisCache {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
    @Autowired
    private RedisConnection redisConnection;

    @Autowired
    private ValueOperations<String, String> valueOperations;
    @Autowired
    private SetOperations<String, Object> setOperations;
    @Autowired
    private HashOperations<String, String, Object> hashOperations;
    @Autowired
    private ListOperations<String, Object> listOperations;
    @Autowired
    private ZSetOperations<String, Object> zSetOperations;
    @Autowired
    private HyperLogLogOperations<String, Object> hyperLogLogOperations;
    @Autowired
    private GeoOperations<String, Object> geoOperations;

    @Autowired
    private RedisProperties redisProperties;


    /**
     * key 是否存在
     */
    public boolean hasKey(String key) {
        return redisTemplate.hasKey(key);
    }

    /**
     * key 删除 注：hash类型会删除整个集合
     */
    public boolean delete(String key) {
        return redisTemplate.delete(key);
    }

    /**
     * 设置key过期时间 所有类型 单位：毫秒
     */
    public boolean expire(String key, Long time) {
        return redisTemplate.expire(key, time, TimeUnit.MILLISECONDS);
    }

    /**
     * 设置过期日期
     */
    public boolean expireAt(String key, Date date) {
        return redisTemplate.expireAt(key, date);
    }

    /**
     * 添加string类型 无过期时间
     */
    public void set(String key, String value) {
        redisTemplate.opsForValue().set(key, value);
    }

    /**
     * 添加string类型 有过期时间
     */
    public void set(String key, String value, long timeOut) {
        valueOperations.set(key, value, timeOut, TimeUnit.MILLISECONDS);
    }

    /**
     * string类型 获取
     */
    public String get(String key) {
        return valueOperations.get(key);
    }

    /**
     * 添加对象
     */
    public <T> void setObject(final String key, final T value) {
        redisTemplate.opsForValue().set(key, value);
    }

    /**
     * 添加对象 带过期时间
     */
    public <T> void setObject(final String key, final T value, long timeOut, TimeUnit timeUnit) {
        redisTemplate.opsForValue().set(key, value, timeOut, timeUnit);
    }

    /**
     * 获取对象
     */
    public Object getObject(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    /**
     * 数值 + value
     */
    public Long incrBy(String key, Long value) {
        return valueOperations.increment(key, value);
    }

    /**
     * 数值 + 1
     */
    public Long incr(String key) {
        return valueOperations.increment(key);
    }

    /**
     * 数值 - value
     */
    public Long decrBy(String key, Long value) {
        return valueOperations.decrement(key, value);
    }

    /**
     * 数值 - 1
     */
    public Long decr(String key) {
        return valueOperations.decrement(key);
    }

    /**
     * 添加 bitmap
     */
    public Boolean setBit(String key, long offset, Boolean tag) {
        return valueOperations.setBit(key, offset, tag);
    }

    /**
     * 获取 bitmap
     */
    public Boolean getBit(String key, long offset) {
        return valueOperations.getBit(key, offset);
    }

    /**
     * bitmap AND 逻辑与、 OR 逻辑或、 NOT 逻辑非、 XOR 逻辑异或
     *
     * @param destination 目标key
     * @param sourceKeys  源数据
     */
    public Long bitOp(RedisStringCommands.BitOperation op, String destination, String... sourceKeys) {
        byte[][] bytes = new byte[sourceKeys.length][];
        for (int i = 0; i < sourceKeys.length; i++) {
            bytes[i] = sourceKeys[i].getBytes();
        }
        return redisTemplate.execute((RedisCallback<Long>) con -> con.bitOp(op, destination.getBytes(), bytes));
    }

    /**
     * bitmap 统计
     */
    public Long bitCount(String key) {
        return redisConnection.bitCount(key.getBytes());
    }

    /**
     * bitmap 统计 跟字节下标   0 -1 统计所有
     */
    public Long bitCount(String key, long start, long end) {
        return redisConnection.bitCount(key.getBytes(), start, end);
    }

    /**
     * hash 添加
     */
    public void hash(String key, Map map) {
        hashOperations.putAll(key, map);
    }

    /**
     * 获取hash 所有key
     *
     * @return
     */
    public Set<String> getHashKeys(String key) {
        return hashOperations.keys(key);
    }

    /**
     * 获取hash单值
     */
    public Object getHashOne(String key, String hashKey) {
        return hashOperations.get(key, hashKey);
    }

    /**
     * hash key 是否存在
     */
    public Boolean checkHashKey(String key, String hashKey) {
        return hashOperations.hasKey(key, hashKey);
    }

    /**
     * 获取hash 所有value
     */
    public void getHashValues(String key) {
        hashOperations.values(key);
    }

    /**
     * 添加set类型
     */
    public Long addSet(String key, String o) {
        return setOperations.add(key, o);
    }

    /**
     * 获取set类型 成员
     */
    public Set<Object> getSetMembers(String key) {
        return setOperations.members(key);
    }

    /**
     * set类型 取差集
     */
    public Set<Object> differenceSet(String key, String key2) {
        Set<Object> set = setOperations.difference(key, key2);
        return set;
    }

    /**
     * set类型 取差集（多个）
     */
    public Set<Object> differenceSets(String key, List<String> keys) {
        Set<Object> set = setOperations.difference(key, keys);
        return set;
    }

    /**
     * set类型 取交集
     */
    public Set<Object> intersectSet(String key, String key2) {
        Set<Object> set = setOperations.intersect(key, key2);
        return set;
    }

    /**
     * set类型 取交集（多个）
     */
    public Set<Object> intersectSets(String key, List<String> keys) {
        Set<Object> set = setOperations.intersect(key, keys);
        return set;
    }

    /**
     * list类型  右侧添加
     */
    public Long addRightList(String key, List<Object> o) {
        return listOperations.rightPushAll(key, o);
    }

    /**
     * list类型  左侧添加
     */
    public Long addLeftList(String key, List<Object> o) {
        return listOperations.leftPushAll(key, o);
    }

    /**
     * list类型 可用于分页
     *
     * @param start 可为负数 从尾部计数
     * @param end   可为负数
     */
    public List<Object> rangeList(String key, long start, long end) {
        return listOperations.range(key, start, end);
    }

    /**
     * zset类型 添加
     */
    public Boolean zset(String key, String o, Long score) {
        return zSetOperations.add(key, o, score);
    }

    /**
     * zset类型 根据分值倒序
     */
    public Set<Object> reverseRangeZset(String key, Long start, Long end) {
        Set<Object> set = zSetOperations.reverseRange(key, start, end);
        return set;
    }

    /**
     * zset类型 范围查找
     */
    public Set<Object> rangeZset(String key, Long start, Long end) {
        Set<Object> set = zSetOperations.range(key, start, end);
        return set;
    }

    /**
     * hyperLogLog类型 添加
     */
    public Long addHyperLogLog(String key, String... values) {
        return hyperLogLogOperations.add(key, values);
    }

    /**
     * hyperLogLog类型 合并
     *
     * @param destination 目标key
     * @param sourceKeys  源数据
     */
    public Long unionHyperLogLog(String destination, String... sourceKeys) {
        return hyperLogLogOperations.union(destination, sourceKeys);
    }

    /**
     * hyperLogLog类型 统计
     */
    public Long hyperLogLogSize(String key) {
        return hyperLogLogOperations.size(key);
    }

    /**
     * geo类型 添加
     *
     * @param key
     * @param point  经纬度
     * @param member 成员
     */
    public Long addGeo(String key, Point point, String member) {
        return geoOperations.add(key, point, member);
    }

    /**
     * geo类型 获取某个或多个地理位置的坐标
     */
    public List<Point> getGeo(String key, String... member) {
        return geoOperations.position(key, member);
    }

    /**
     * geo类型 获取两个成员的距离 单位米
     */
    public Distance distanceGeo(String key, String member, String member2) {
        return geoOperations.distance(key, member, member2);
    }

    /**
     * 管道 批量查询
     */
    public List<Object> pipelined(List<String> keys) {
        List<Object> list = redisTemplate.executePipelined(new RedisCallback<String>() {
            @Override
            public String doInRedis(RedisConnection connection) throws DataAccessException {
                for (String key : keys) {
                    connection.get(key.getBytes());
                }
                // RedisCallback返回值必须为null，因为为了支持返回管道命令的结果而舍弃了该值。
                return null;
            }
        });
        return list;
    }

    /**
     * 获取有效期 单位秒
     */
    public long getValidity(String key) {

        if (redisTemplate.hasKey(key)) {
            String uri = "redis://" + "@" + redisProperties.getHost() + ":" + redisProperties.getPort() + "/0";
            if (!StringUtils.isEmpty(redisProperties.getPassword())) {
                uri = "redis://" + redisProperties.getPassword() + "@" + redisProperties.getHost() + ":" + redisProperties.getPort() + "/0";
            }
            RedisClient redisClient = RedisClient.create(uri);
            try (StatefulRedisConnection<String, String> connection = redisClient.connect()) {
                RedisAsyncCommands<String, String> commands = connection.async();
                RedisFuture<Long> ttl = commands.ttl(key);
                return ttl.get();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return 0;
    }
}
