package com.redisdatastorage.cache;

import org.redisson.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * RedissonUtil
 *
 * @author chenjing
 */
@Component
public class RedissonLock {

    @Autowired
    private RedissonClient redissonClient;

    /**
     * 分布式可重入锁 支持 TTL、异步
     */
    public RLock lock(String key) {
        return redissonClient.getLock(key);
    }

    /**
     * 分布式可重入公平锁 支持 TTL、异步
     * 以请求的顺序获得锁
     */
    public RLock fairLock(String key) {
        return redissonClient.getFairLock(key);
    }

    /**
     * 分布式可重入读写锁 支持TTL
     * 可以同时存在多个 ReadLock 拥有者，但仅允许有一个 WriteLock
     */
    public RReadWriteLock readWriteLock(String key) {
        return redissonClient.getReadWriteLock(key);
    }

    /**
     * 信号量 可做流量限制
     */
    public RSemaphore semaphore(String key) {
        return redissonClient.getSemaphore(key);
    }

    /**
     * 倒计时 等待足够线程执行后，执行后续操作
     */
    public RCountDownLatch countDownLatch(String key) {
        return redissonClient.getCountDownLatch(key);
    }
}
