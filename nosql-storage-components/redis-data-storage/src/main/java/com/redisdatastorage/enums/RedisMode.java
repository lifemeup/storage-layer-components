package com.redisdatastorage.enums;

/**
 * RedisMode
 *
 * @author chenjing
 */
public enum RedisMode {

    STANDALONE("standalone"),
    CLUSTER("cluster"),
    SENTINEL("sentinel"),
    ;

    RedisMode(String mode) {
        this.mode = mode;
    }

    private String mode;

    public String getMode() {
        return mode;
    }
}
