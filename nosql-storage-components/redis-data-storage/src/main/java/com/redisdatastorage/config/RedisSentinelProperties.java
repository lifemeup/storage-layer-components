package com.redisdatastorage.config;

import org.redisson.config.ReadMode;

import java.util.List;

/**
 * RedisSentinelProperties
 *
 * @author chenjing
 */
public class RedisSentinelProperties {

    /**
     * 服务名称
     */
    private String master;
    /**
     * 哨兵相关服务
     */
    private List<String> nodes;
    /**
     * 读取模式 SLAVE、MASTER、MASTER-SLAVE
     */
    private ReadMode readMode = ReadMode.MASTER;

    public List<String> getNodes() {
        return nodes;
    }

    public void setNodes(List<String> nodes) {
        this.nodes = nodes;
    }

    public String getMaster() {
        return master;
    }

    public void setMaster(String master) {
        this.master = master;
    }

    public ReadMode getReadMode() {
        return readMode;
    }

    public void setReadMode(ReadMode readMode) {
        this.readMode = readMode;
    }
}
