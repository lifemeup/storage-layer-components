package com.redisdatastorage.config;

import org.redisson.config.ReadMode;

import java.util.List;

/**
 * RedisClusterProperties
 *
 * @author chenjing
 */
public class RedisClusterProperties {

    /**
     * 集群相关服务 ip:port
     */
    private List<String> nodes;
    /**
     * 最大重定向
     */
    private Integer maxRedirects = 5;
    /**
     * 读取模式 SLAVE、MASTER、MASTER-SLAVE
     */
    private ReadMode readMode = ReadMode.MASTER;

    public List<String> getNodes() {
        return nodes;
    }

    public void setNodes(List<String> nodes) {
        this.nodes = nodes;
    }

    public Integer getMaxRedirects() {
        return maxRedirects;
    }

    public void setMaxRedirects(Integer maxRedirects) {
        this.maxRedirects = maxRedirects;
    }

    public ReadMode getReadMode() {
        return readMode;
    }

    public void setReadMode(ReadMode readMode) {
        this.readMode = readMode;
    }
}
