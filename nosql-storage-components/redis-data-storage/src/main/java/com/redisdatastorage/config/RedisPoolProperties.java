package com.redisdatastorage.config;

/**
 * RedisPoolProperties
 *
 * @author chenjing
 */
public class RedisPoolProperties {

    /**
     * 是否使用连接池
     */
    private boolean enable = false;
    /**
     * 最大连接数
     */
    private Integer maxActive = 8;
    /**
     * 最大等待时间 毫秒 负数表示无限制
     */
    private Integer maxWait = -1;
    /**
     * 最大空闲连接
     */
    private Integer maxIdle = 8;
    /**
     * 最小空闲连接
     */
    private Integer minIdle = 0;


    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public Integer getMaxActive() {
        return maxActive;
    }

    public void setMaxActive(Integer maxActive) {
        this.maxActive = maxActive;
    }

    public Integer getMaxWait() {
        return maxWait;
    }

    public void setMaxWait(Integer maxWait) {
        this.maxWait = maxWait;
    }

    public Integer getMaxIdle() {
        return maxIdle;
    }

    public void setMaxIdle(Integer maxIdle) {
        this.maxIdle = maxIdle;
    }

    public Integer getMinIdle() {
        return minIdle;
    }

    public void setMinIdle(Integer minIdle) {
        this.minIdle = minIdle;
    }
}
