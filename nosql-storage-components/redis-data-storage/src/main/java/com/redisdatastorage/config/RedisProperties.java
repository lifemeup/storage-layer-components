package com.redisdatastorage.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;


/**
 * RedisProperties
 *
 * @author chenjing
 */
@ConfigurationProperties(prefix = "redis")
public class RedisProperties {

    /**
     * 模式 standalone/cluster/sentinel
     */
    private String mode = "standalone";
    /**
     * 数据库
     */
    private Integer database = 0;
    /**
     * 密码 无则不写
     */
    private String password;
    /**
     * 连接超时时间 毫秒
     */
    private Integer timeout = 5000;
    /**
     * 单机 ip
     */
    private String host = "localhost";
    /**
     * 端口
     */
    private Integer port = 6379;
    /**
     * 连接池
     */
    @NestedConfigurationProperty
    private RedisPoolProperties pool;

    /**
     * 集群
     */
    @NestedConfigurationProperty
    private RedisClusterProperties cluster;
    /**
     * 哨兵
     */
    @NestedConfigurationProperty
    private RedisSentinelProperties sentinel;

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public Integer getDatabase() {
        return database;
    }

    public void setDatabase(Integer database) {
        this.database = database;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getTimeout() {
        return timeout;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public RedisPoolProperties getPool() {
        return pool;
    }

    public void setPool(RedisPoolProperties pool) {
        this.pool = pool;
    }

    public RedisClusterProperties getCluster() {
        return cluster;
    }

    public void setCluster(RedisClusterProperties cluster) {
        this.cluster = cluster;
    }

    public RedisSentinelProperties getSentinel() {
        return sentinel;
    }

    public void setSentinel(RedisSentinelProperties sentinel) {
        this.sentinel = sentinel;
    }
}
